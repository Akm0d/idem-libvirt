import libvirt
from xml.etree import ElementTree
from typing import List, Any, Tuple

__func_alias__ = {"list_": "list"}

VIRT_STATE_NAME_MAP = {
    0: "running",
    1: "running",
    2: "running",
    3: "paused",
    4: "shutdown",
    5: "shutdown",
    6: "crashed",
}

IP_LEARNING_XML = """<filterref filter='clean-traffic'>
        <parameter name='CTRL_IP_LEARNING' value='any'/>
      </filterref>"""


def list_(hub, ctx):
    """
    Return a list of the VMs

    id (str)
    image (str)
    size (str)
    state (str)
    private_ips (list)
    public_ips (list)
    """
    ret = {}
    conn = ctx.acct.connection
    domains = conn.listAllDomains()
    for domain in domains:
        data = {
            "id": domain.UUIDString(),
            "image": "",
            "size": "",
            "state": VIRT_STATE_NAME_MAP[domain.state()[0]],
            "private_ips": [],
            "public_ips": "TODO, get this from hub.exec.libvirt.domain.ip.list()",
        }
        # TODO: Annoyingly name is not guaranteed to be unique, but the id will not work in other places
        ret[domain.name()] = data

    return ret


def _get_clone_domain(hub, ctx, name: str, base: str) -> Tuple[Any, List[Any]]:
    cleanup = []
    conn: libvirt.virConnect = ctx.acct.connection

    domain = conn.lookupByName(base)
    # TODO: ensure base is shut down before cloning
    xml_ = domain.XMLDesc(0)

    hub.log.debug(f"Source machine XML '{xml_}'")

    domain_xml = ElementTree.fromstring(xml_)
    domain_xml.find("./name").text = name
    if domain_xml.find("./description") is None:
        description_elem = ElementTree.Element("description")
        domain_xml.insert(0, description_elem)
    description = domain_xml.find("./description")
    description.text = f"Cloned from {base}"
    domain_xml.remove(domain_xml.find("./uuid"))

    for iface_xml in domain_xml.findall("./devices/interface"):
        iface_xml.remove(iface_xml.find("./mac"))
        # enable IP learning, this might be a default behaviour...
        # Don't always enable since it can cause problems through libvirt-4.5
        if (
            ctx.acct.ip_source == "ip-learning"
            and iface_xml.find("./filterref/parameter[@name='CTRL_IP_LEARNING']")
            is None
        ):
            iface_xml.append(ElementTree.fromstring(IP_LEARNING_XML))

    # If a qemu agent is defined we need to fix the path to its socket
    # <channel type='unix'>
    #   <source mode='bind' path='/var/lib/libvirt/qemu/channel/target/domain-<dom-name>/org.qemu.guest_agent.0'/>
    #   <target type='virtio' name='org.qemu.guest_agent.0'/>
    #   <address type='virtio-serial' controller='0' bus='0' port='2'/>
    # </channel>
    for agent_xml in domain_xml.findall("""./devices/channel[@type='unix']"""):
        #  is org.qemu.guest_agent.0 an option?
        if (
            agent_xml.find(
                """./target[@type='virtio'][@name='org.qemu.guest_agent.0']"""
            )
            is not None
        ):
            source_element = agent_xml.find("""./source[@mode='bind']""")
            # see if there is a path element that needs rewriting
            if source_element and "path" in source_element.attrib:
                path = source_element.attrib["path"]
                new_path = path.replace(
                    "/domain-{}/".format(base), "/domain-{}/".format(name)
                )
                hub.log.debug("Rewriting agent socket path to %s", new_path)
                source_element.attrib["path"] = new_path

    for disk in domain_xml.findall("""./devices/disk[@device='disk'][@type='file']"""):
        # print "Disk: ", ElementTree.tostring(disk)
        # check if we can clone
        driver = disk.find("./driver[@name='qemu']")
        if driver is None:
            # Err on the safe side
            raise OSError("Non qemu driver disk encountered bailing out.")
        disk_type = driver.attrib.get("type")
        hub.log.info("disk attributes %s", disk.attrib)
        if disk_type == "qcow2":
            source = disk.find("./source").attrib["file"]
            pool, volume = find_pool_and_volume(conn, source)
            if ctx.acct.clone_strategy == "quick":
                new_volume = pool.createXML(
                    create_volume_with_backing_store_xml(volume), 0
                )
            else:
                new_volume = pool.createXMLFrom(create_volume_xml(volume), volume, 0)
            cleanup.append({"what": "volume", "item": new_volume})

            disk.find("./source").attrib["file"] = new_volume.path()
        elif disk_type == "raw":
            source = disk.find("./source").attrib["file"]
            pool, volume = find_pool_and_volume(conn, source)
            # TODO: more control on the cloned disk type
            new_volume = pool.createXMLFrom(create_volume_xml(volume), volume, 0)
            cleanup.append({"what": "volume", "item": new_volume})

            disk.find("./source").attrib["file"] = new_volume.path()
        else:
            raise OSError(f"Disk type '{disk_type}' not supported")

    clone_xml = ElementTree.tostring(domain_xml).decode()
    hub.log.debug("Clone XML '%s'", clone_xml)

    validate_flags = libvirt.VIR_DOMAIN_DEFINE_VALIDATE if ctx.acct.validate_xml else 0
    clone_domain = conn.defineXMLFlags(clone_xml, validate_flags)

    cleanup.append({"what": "domain", "item": clone_domain})
    clone_domain.createWithFlags(libvirt.VIR_DOMAIN_START_FORCE_BOOT)

    return clone_domain, cleanup


def _do_cleanup(hub, cleanup: List[Any]):
    """
    Clean up clone domain leftovers as much as possible.

    Extra robust clean up in order to deal with some small changes in libvirt
    behavior over time. Passed in volumes and domains are deleted, any errors
    are ignored. Used when cloning/provisioning a domain fails.

    :param cleanup: list containing dictonaries with two keys: 'what' and 'item'.
                    If 'what' is domain the 'item' is a libvirt domain object.
                    If 'what' is volume then the item is a libvirt volume object.

    Returns:
        none
    """
    hub.log.info("Cleaning up after exception")
    for leftover in cleanup:
        what = leftover["what"]
        item = leftover["item"]
        if what == "domain":
            hub.log.info("Cleaning up %s %s", what, item.name())
            try:
                item.destroy()
                hub.log.debug("%s %s forced off", what, item.name())
            except libvirt.libvirtError:
                pass
            try:
                item.undefineFlags(
                    libvirt.VIR_DOMAIN_UNDEFINE_MANAGED_SAVE
                    + libvirt.VIR_DOMAIN_UNDEFINE_SNAPSHOTS_METADATA
                    + libvirt.VIR_DOMAIN_UNDEFINE_NVRAM
                )
                hub.log.debug("%s %s undefined", what, item.name())
            except libvirt.libvirtError:
                pass
        if what == "volume":
            try:
                item.delete()
                hub.log.debug("%s %s cleaned up", what, item.name())
            except libvirt.libvirtError:
                pass


async def create(hub, ctx, name: str):
    """
    Provision a single machine
    """
    # TODO: check name qemu/libvirt will choke on some characters (like '/')?

    hub.log.info(
        f"Cloning '{name}' with strategy '{ctx.acct.clone_strategy}' validate_xml='{ctx.acct.validate_xml}'",
    )

    conn: libvirt.virConnect = ctx.acct.connection
    base = ctx.acct.base_domain
    cleanup = []

    try:
        # clone the vm
        try:
            clone_domain = conn.lookupByName(name)
        except libvirt.libvirtError as e:
            hub.log.error(f"libvirtError: {e}")
            clone_domain, cleanup_ext = _get_clone_domain(hub, ctx, name, base)
            cleanup.extend(cleanup_ext)

        if ctx.acct.ip_source == "qemu-agent":
            ip_source = libvirt.VIR_DOMAIN_INTERFACE_ADDRESSES_SRC_AGENT
        else:  # ctx.acct.ip_source == "ip-learning"
            ip_source = libvirt.VIR_DOMAIN_INTERFACE_ADDRESSES_SRC_LEASE

        address = await hub.tool.cloud.network.wait_for_ip(
            hub.tool.libvirt.domain.ip.get, update_args=(clone_domain, 0, ip_source),
        )

        hub.log.info(f"Address = {address}")

        # TODO bootstrap the node and return the result
        return {}
    except Exception:
        _do_cleanup(hub, cleanup)
        # throw the root cause after cleanup
        raise
