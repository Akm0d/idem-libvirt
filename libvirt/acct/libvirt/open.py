import libvirt
from dict_tools import data
from typing import Any, Dict


async def gather(hub) -> Dict[str, Any]:
    """

    Example:
    .. code-block:: yaml

        libvirt.open:
          profile_name:
              # url: "qemu+ssh://user@remotekvm/system?socket=/var/run/libvirt/libvirt-sock"
              url: qemu:///system
              base_domain: base-image
              ip_source: ip-learning # [ ip-learning | qemu-agent ]
              clone_strategy: quick # [ quick | full ]
              ssh_private_key: None
              ssh_username: vagrant
              ssh_password: vagrant

    """
    sub_profiles = {}

    for profile, ctx in hub.acct.PROFILES.get("libvirt.client", {}).items():
        sub_profiles[profile] = data.NamespaceDict(
            connection=libvirt.open(ctx["url"]),
            private_key=ctx["private_key"],
            base_domain=ctx["base_domain"],
            clone_strategy=ctx.get("clone_strategy", "full"),
            ip_source=ctx.get("ip_source", "ip-learning"),
            validate_xml=ctx.get("validate_xml", True),
        )
        hub.tool.libvirt.profile.validate(sub_profiles[profile])
    return sub_profiles
