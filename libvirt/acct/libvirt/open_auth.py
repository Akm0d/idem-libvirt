import libvirt
from dict_tools import data
from typing import Any, Dict


async def gather(hub) -> Dict[str, Any]:
    """

    Example:
    .. code-block:: yaml

        libvirt.open_auth:
          profile_name:
              # url: "qemu+ssh://user@remotekvm/system?socket=/var/run/libvirt/libvirt-sock"
              url: qemu:///system
              base_domain: base-image
              ip_source: ip-learning # [ ip-learning | qemu-agent ]
              clone_strategy: quick # [ quick | full ]
              sasl_username: vagrant
              sasl_password: vagrant

    """
    sub_profiles = {}

    for profile, ctx in hub.acct.PROFILES.get("libvirt.client", {}).items():

        def _request_cred(credentials, user_data):
            for credential in credentials:
                if credential[0] == libvirt.VIR_CRED_AUTHNAME:
                    credential[4] = ctx["sasl_username"]
                elif credential[0] == libvirt.VIR_CRED_PASSPHRASE:
                    credential[4] = ctx["sasl_password"]
            return 0

        sub_profiles[profile] = data.NamespaceDict(
            connection=libvirt.openAuth(
                ctx["url"],
                auth=[
                    [libvirt.VIR_CRED_AUTHNAME, libvirt.VIR_CRED_PASSPHRASE],
                    _request_cred,
                    None,
                ],
            ),
            private_key=ctx["private_key"],
            base_domain=ctx["base_domain"],
            clone_strategy=ctx.get("clone_strategy", "full"),
            ip_source=ctx.get("ip_source", "ip-learning"),
            validate_xml=ctx.get("validate_xml", True),
        )
        hub.tool.libvirt.profile.validate(sub_profiles[profile])
    return sub_profiles
