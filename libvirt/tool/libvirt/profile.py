from dict_tools import data
import os


def validate(hub, profile: data.NamespaceDict):
    """
    Verify that a libvirt profile has all of the keys and attributes necessary
    """
    if profile.clone_strategy not in ("quick", "full"):
        raise ValueError(
            f"'clone_strategy' must be one of quick or full. Got '{profile.clone_strategy}'"
        )

    if profile.ip_source not in ("ip-learning", "qemu-agent"):
        raise ValueError(
            f"'ip_source' must be one of qemu-agent or ip-learning. Got '{profile.ip_source}'"
        )

    assert os.path.exists(
        profile.private_key
    ), f"The defined key_filename '{profile.private_key}' does not exist"
