import libvirt
from typing import List

__func_alias__ = {"list_": "list"}


def to_ip_addr_type(addr_type) -> str:
    if addr_type == libvirt.VIR_IP_ADDR_TYPE_IPV4:
        return "ipv4"
    elif addr_type == libvirt.VIR_IP_ADDR_TYPE_IPV6:
        return "ipv6"

    raise TypeError(f"Unknown ipaddr type: {addr_type}")


def list_(hub, domain, ip_source) -> List[str]:
    ips = []
    state = domain.state(0)
    if state[0] != libvirt.VIR_DOMAIN_RUNNING:
        return ips
    try:
        addresses = domain.interfaceAddresses(ip_source, 0)
    except libvirt.libvirtError as error:
        hub.log.info("Exception polling address %s", error)
        return ips

    for (name, val) in addresses.items():
        if val["addrs"]:
            for addr in val["addrs"]:
                tp = hub.tool.libvirt.domain.ip.to_ip_addr_type(addr["type"])
                hub.log.info("Found address %s", addr)
                if tp == "ipv4":
                    ips.append(addr["addr"])
    return ips


def get(hub, domain, idx, ip_source, skip_loopback: bool = True):
    ips = hub.tool.libvirt.domain.ip.list(domain, ip_source)

    if skip_loopback:
        ips = [ip for ip in ips if not ip.startswith("127.")]

    if not ips or len(ips) <= idx:
        return None

    return ips[idx]
